const PORT       = 3003;
const HOST       = `localhost:${PORT}`;
const RAILS_HOST = 'localhost:3000';

const { createServer } = require('http');
const { parse }        = require('url');
const next             = require('next');
const httpProxy        = require('http-proxy');

// The Frontend (Node.js) server (running next.js) will proxy Auth and
// API Data requests to the Backend (Rails) server.
const proxy = httpProxy.createProxyServer({
  // Automatically rewrite the `location` header so redirect requests
  // (ex. login's auth/session redirect response) redirects back to
  // the Frontend (Node.js) server
  autoRewrite: true,

  // Make sure cookies are written with the Frontend (Node.js) server
  cookieDomainRewrite: HOST
});
const dev    = process.env.NODE_ENV !== 'production';
const app    = next({ dev });
const handle = app.getRequestHandler();

(async () => {
  await app.prepare();

  // Create the Frontend (Node.js) server
  const server = createServer((req, res) => {
    const { pathname, query } = parse(req.url, true);
    let match;

    // Proxy Auth and API Data requests to the Backend (Rails) server
    if (/\/api\//.test(pathname) || /\/auth\//.test(pathname) || /\/client\//.test(pathname)) {
      proxy.web(req, res, { target: `http://${RAILS_HOST}` });
      return;
    }
    else if(pathname === '/campaigns' && query.id) {
      app.render(req, res, '/campaigns', query);
      return;
    }

    // ... Otherwise allow Frontend (Node.js) server to handle the
    // the request.  Specifically, allow the Next.js framework.
    handle(req, res);
  });

  server.listen(PORT, err => {
    if (err) throw err;
    console.log(`> Ready on http://${HOST}`);
  });
})();
