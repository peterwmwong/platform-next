import './decimal';
import moment from 'moment';
import _ from 'lodash';

function pad(n) {
  var r = String(n);

  if (r.length === 1) {
    r = '0' + r;
  }

  return r;
}

function normalizeDate(date){
  if(typeof date === 'string' && /^\d\d\d\d-\d\d-\d\d$/.test(date)){
    return new Date(`${date} 00:00:00`);
  }
  return date;
}

var formatters = {
  number: function(num, opts) {
    var s, parts;
    opts = _.defaults(opts || {}, { ifNull: '--', delimiter: ',', separator: '.', precision: null });
    if (typeof num !== 'number') { return opts.ifNull; }
    s = opts.precision != null ?
      Math.round10(num, -opts.precision).toFixed(opts.precision) : num.toString();
    parts = s.split('.');
    parts[0] = parts[0].replace(/(\d)(?=(\d{3})+$)/g, "$1" + (opts.delimiter || ''));
    return parts[1] ? parts[0] + opts.separator + parts[1] : parts[0];
  },

  integer: function(num, opts) {
    opts = _.assign(_.defaults(opts || {}, { ifNull: '--', delimiter: ',' }), { precision: 0 });
    return formatters.number(num, opts);
  },

  currency: function(num, opts) {
    opts = _.defaults(opts || {}, { ifNull: '--', delimiter: ',', precision: 2, unit: '$' });
    if (typeof num !== 'number') { return opts.ifNull; }

    var unit = opts.unit || '';
    var signAndUnit = num < 0 ? "-" + unit : unit;
    num = Math.abs(num);

    if (opts.precision === 'k') {
      if (num >= 1000) {
        return signAndUnit + formatters.number((num / 1000), { precision:1 }).replace('.0', '') + 'k';
      }
      else {
        return signAndUnit + formatters.number(num, { precision:0 });
      }
    }

    return signAndUnit + formatters.number(num, opts);
  },

  percentage: function(num, opts) {
    opts = _.defaults(opts || {}, { ifNull: '--', delimiter: '', precision: 1 });
    if (typeof num !== 'number') { return opts.ifNull; }
    return formatters.number(num * 100, opts) + '%';
  },

  date: function(date, opts) {
    opts = _.defaults(opts || {}, { ifNull: '--' });
    date = normalizeDate(date);
    if (!(date instanceof Date)) { return opts.ifNull; }

    return moment(date).format(opts.format || 'MM/DD/YY');
  },

  monthDay: function(date, opts) {
    opts = _.defaults(opts || {}, { ifNull: '--' });
    date = normalizeDate(date);
    if (!(date instanceof Date)) { return opts.ifNull; }
    return moment(date).format('MMM D');
  },

  fullMonthDay: function(date, opts) {
    opts = _.defaults(opts || {}, { ifNull: '--' });
    date = normalizeDate(date);
    if (!(date instanceof Date)) { return opts.ifNull; }
    return moment(date).format('MMMM D');
  },

  monthDayYear: function(date, opts) {
    opts = _.defaults(opts || {}, { ifNull: '--' });
    date = normalizeDate(date);
    if (!(date instanceof Date)) { return opts.ifNull; }
    return moment(date).format('MMM D YYYY');
  },

  monthYear: function(date, opts) {
    opts = _.defaults(opts || {}, { ifNull: '--' });
    date = normalizeDate(date);
    if (!(date instanceof Date)) { return opts.ifNull; }
    return moment(date).format('MMM YYYY');
  },

  timeOnMonthDay: function(date) {
    date = normalizeDate(date);
    if (!(date instanceof Date)) return '--';
    return moment(date).format('h:mma') + ' on ' + this.monthDay(date);
  },

  monthDayAtTime: function(date) {
    date = normalizeDate(date);
    if (!(date instanceof Date)) return '--';
    return this.fullMonthDay(date) + ' at ' + moment(date).format('h:mma');
  },

  time: function(date, opts) {
    opts = _.defaults(opts || {}, { format: 'h:mm a' });
    date = normalizeDate(date);
    return moment(date).format(opts.format);
  },

  duration: function(seconds, opts) {
    var h, m, s;

    opts = _.defaults(opts || {}, { ifNull: '--' });

    if (typeof seconds !== 'number') { return opts.ifNull; }

    h = Math.floor(seconds / 3600);
    m = Math.floor((seconds % 3600) / 60);
    s = (seconds % 3600) % 60;

    if (h) {
      return h + ':' + pad(m) + ':' + pad(s);
    }
    else if (m) {
      return m + ':' + pad(s);
    }
    else {
      return ':' + pad(s);
    }
  },

  named: function(object, opts) {
    opts = _.defaults(opts || {}, { ifNull: '' });
    return object ? (object.name || object.description || opts.ifNull) : opts.ifNull;
  },

  phone: function(num) {
    if (!num) { return null; }
    num = num.replace(/[^0-9]/g, '');
    num = num.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    return num;
  },

  // Public: Escapes html in the given string. This should be used whenever unsanitized text is
  // rendered into the DOM using the `dangerouslySetInnerHTML` attribute.
  escapeHTML: function(s) {
    var chr = { '"': '&quot;', '&': '&amp;', '<': '&lt;', '>': '&gt;' };
    return s.replace(/[\"&<>]/g, function(a) { return chr[a]; });
  },

  // Public: Formats a string using simple HTML formatting rules. Very similar to the Rails
  // `simple_format` method.
  //
  // s - The string to format.
  //
  // Returns an HTML formatted string.
  simpleHTML: function(s) {
    s = formatters.escapeHTML(s || '')
      .replace(/\r\n?/g, '\n')                     // \r\n and \r -> \n
      .replace(/(\n\n+)/g, '</p>$1<p>')            // 2+ newline  -> paragraph
      .replace(/([^\n]\n)(?=[^\n])/g, '$1<br />'); // 1 newline   -> br

    return '<p>' + s + '</p>';
  }
};

module.exports = formatters;
