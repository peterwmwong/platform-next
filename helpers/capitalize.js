// Public: Capitalizes the first letter of the given string.
export default s =>
  typeof s === 'string' && s.length ? String.fromCharCode(s.charCodeAt(0) - 32) + s.slice(1) : s;