import _ from 'lodash';

// Public: takes a list of minWidths and returns style objects with widths converted to
// percentages. Also corrects for negative margins by using CSS calc().

export default function(minWidths, options) {
  const opts = _.defaults(options || {}, {
    adjustMargin: true,
    marginAdjustment: 1,
    leftGutter: 0
  });

  const totalWidth = minWidths.reduce((previous, current) => previous + current);

  return minWidths.map((minWidth, index) => {
    const useCalc = index !== 0 && opts.adjustMargin;
    const widthPercent = (minWidth / totalWidth * 100) + '%';
    const marginAdjustment = `${opts.marginAdjustment}px`;
    const calculatedWidth = useCalc ? `calc(${widthPercent} + ${marginAdjustment})` : widthPercent;

    return {
      width: calculatedWidth
    };
  });
};
