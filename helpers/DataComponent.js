/*

Convenience wrapper for declaring a data driven Component.
Once mounted (`componentDidMount`), fetches data (`get data callback`),
and finally renders the fetched data.
This is specifically for components that will fetch data on the client
side once mounted (NOT server side rendered). This is useful for
components displaying data not critical for page load.

getDataOrApiPath - Function or API Path (String).
                   If a function, it is called by `componentDidMount()` and
                   expected to return a promise resolving to data that will be
                   passed to the `render` function (see below).
                   If a string (API Path), automatically fetches data at
                   API Path and passes it to the `render` function (see below).
render - Function rendering page.  Passed one argument, the data fetched by
         `getDataOrAPI` (see above).

Example:

```js
export default DataComponent(
  ({ get }) => get('api/todos/'),
  (todos = []) =>
    <div>
      <h1>Todos</h1>
      <ul>
        {todos.map(t => (
          <li key={t.id}>{t.text}</li>
        ))}
      </ul>
    </div>
)
```

*/
import React from 'react';
import get from './get.js';

// As this component's data fetching will only happen on the client side,
// always pass `null` to `get()` for the `server` argument.
const clientGet = get.bind(null, null);

export default (getDataOrApiPath, render) => (
  class extends React.Component {
    async componentDidMount() {
      this.setState({
        data: await (
          typeof getDataOrApiPath === 'string'
            ? clientGet(getDataOrApiPath)
            : getDataOrApiPath({ props: this.props, get: clientGet })
        )
      });
    }
    render() { return render(this.state && this.state.data); }
  }
)