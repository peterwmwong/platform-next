/*

Convenience wrapper for declaring a data driven Page Component.
Abstracts fetching data from either the reqRes or Client side.

getDataOrApiPath - Function or API Path (String).
                   If a function, it is called by `getInitialProps()` and
                   expected to return a promise resolving to data that will be
                   passed to the `render` function (see below).
                   If a string (API Path), automatically fetches data at
                   API Path and passes it to the `render` function (see below).
render - Function rendering page.  Passed one argument, the data fetched by
         `getDataOrAPI` (see above).

Example:

```js
export default DataPage(
  ({ get }) => get('api/todos/'),
  (todos = []) =>
    <div>
      <h1>Todos</h1>
      <ul>
        {todo.map(t => (
          <li key={t.id}>{t.text}</li>
        ))}
      </ul>
    </div>
)
```

Is the same as...

```js
export default DataPage(
  'api/todos/',
  (todos = []) =>
    <div>
      <h1>Todos</h1>
      <ul>
        {todo.map(t => (
          <li key={t.id}>{t.text}</li>
        ))}
      </ul>
    </div>
)
```

*/
import Head from 'next/head';
import AppNavbar from '../components/AppNavBar';
import get from './get.js';

export default (getDataOrApiPath, render) => (
  class extends React.Component {
    static async getInitialProps(reqRes) {
      return {
        data: await (
          typeof getDataOrApiPath === 'string'
            ? get(reqRes, getDataOrApiPath)
            : getDataOrApiPath({ reqRes, get: apiPath => get(reqRes, apiPath) })
        )
      };
    }
    render() {
      return (
        <div>
          <Head>
            <link rel="stylesheet" href="/client/styles-46ca1d60291a1bb7592c.css" media="all" />
            <style>{`
              html, body {
                margin: 0;
                padding: 0;
                width: 100%;
                height: 100%;
              }
            `}</style>
          </Head>
          <div className="App">
            <AppNavbar />
            <div className="App-main">
              {render(this.props.data)}
            </div>
          </div>
        </div>
      );
    }
  }
)