import 'isomorphic-fetch';

const API_HOST = 'http://localhost:3003/';

export default async function get(reqRes, apiPath, params) {
  const { req, res } = reqRes || {};
  try {
    const fetchRes  = await fetch(
      new Request(`${API_HOST}${apiPath}`, {
        method: 'GET',
        credentials: 'include',
        headers: new Headers({
          "Accept": "application/json",
          "Cookie": req ? req.headers.cookie : document.cookie
        })
      })
    );
    const isSuccess = fetchRes && fetchRes.status < 300 && fetchRes.status >= 200;
    if(isSuccess) return await fetchRes.json();
  }
  catch(e) {
    /* TODO: Consider logging. Currently Next.js already logs so maybe uncessary... */
  }

  // If not successful (for whatever reason), redirect to `/auth/login`
  // TODO: Let's verify and match the current Platform behavior.
  // Server redirect
  if(req) {
    res.statusCode = 302;
    res.setHeader('location', '/auth/login');
    res.end('');
    return res;
  }
  // Client redirect
  else window.location = '/auth/login';
}