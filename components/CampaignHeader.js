import DataComponent from '../helpers/DataComponent';
import CampaignTitle from './CampaignTitle';
import CampaignStatus from './CampaignStatus';
import CampaignMessageIndicator from './CampaignMessageIndicator';

const BackButton = ({ onClick }) => (
  <div className="CampaignHeader-backButton Flex-fit u-posRelative" style={{ width: 16 }}>
    <div
      className="u-clickable u-posFillContainer u-decoBackgroundOffWhiteShadedMore u-decoBorderRightLightTinted"
      onClick={onClick}
    >
      <div className="u-textColorLight u-posAutoCentered u-textSizeLarge">
        ◂
      </div>
    </div>
  </div>
);

export default DataComponent(
  ({ get, props }) => get(`api/direct/campaign_overviews/${props.campaignId}/header`),
  campaignViewHeader => (
    !campaignViewHeader
      ? null
      : <div className="Flex">
          <div className="Flex-grow">
            <CampaignTitle
              title={campaignViewHeader.name}
              clientName={campaignViewHeader.client_name}
              brandName={campaignViewHeader.brand_name}
              startDate={campaignViewHeader.current_start_date}
              endDate={campaignViewHeader.current_end_date}
              ugcid={campaignViewHeader.ugcid}
            />
          </div>
          <div className="Flex-fit u-decoBorderRightLightTinted">
            <CampaignStatus status={campaignViewHeader.status} />
          </div>
          <div className="Flex-fit u-decoBorderRightLightTinted">
            <CampaignMessageIndicator
              count={campaignViewHeader.total_unread_message_count}
            />
          </div>
        </div>
  )
);

// CampaignHeader.propTypes = {
//   campaignViewHeader: React.PropTypes.object,
//   onToggleMessages: React.PropTypes.func,
//   isShowingMessages: React.PropTypes.bool,
//   onClickBack: React.PropTypes.func
// };
