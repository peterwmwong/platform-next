import cx from 'classnames';
import SVGIcon from './common/SVGIcon';

const CampaignMessageIndicator = ({ count, onToggle, isActive }) => (
  <div
    onClick={onToggle}
    className={cx(
      "MessageCenter-button TopIconButton u-paddingTopBottom2 u-paddingLeftRight1 u-textCenter",
      {
        'is-active': isActive
      }
    )}
    style={{ fontSize: 0 }}
  >
    <div className="u-posRelative">
      { !!count &&
        <span
          className="Counter Counter--withBorder TopIconButton-counter u-posAbsolute"
          style={{ top: 0, right: 0 }}
        >
          {count}
        </span>
      }
      <SVGIcon
        iconName="icon-messages-display"
        className="TopIconButton-icon u-marginBottom1"
        style={{ height: 36, width: 38 }}
      />
    </div>
    <div className="u-textSizeSmall u-textHeight2pt5">
      Messages
    </div>
  </div>
);

CampaignMessageIndicator.propTypes = {
  count: React.PropTypes.number,
  onToggle: React.PropTypes.func,
  isActive: React.PropTypes.bool
};

export default CampaignMessageIndicator;
