import cx from 'classnames';
import SVGIcon from './common/SVGIcon';

export default class AppNavBar extends React.Component {
  constructor(){
    super();
    this.state = {};
  }

  render() {
    const searchNavClasses = cx({
      "App-navItem": true,
      "is-active": this.state.globalSearchEnabled
    });

    const notificationsClasses = cx({
      "App-navItem": true,
      "is-active": this.state.showNotifications,
      "App-navItem--notifications": true
    });

    const documentationClasses = cx({
      "App-navItem": true,
      "is-active": this.state.showDocumentation
    });

    const menuNavClasses = cx({
      'App-navItem': true,
      'App-navItem--menu': true,
      'u-posRelative': true,
      'is-active': this.state.appDrawerEnabled
    });

    return (
      <div className="App-header">
        <a
          className={menuNavClasses}
          title="Menu"
        >
          <span className="u-hiddenVisually">Menu</span>
          <SVGIcon className="App-icon" iconName="icon-menu"/>
        </a>

        <a href="/" title="Home" className="u-inlineBlock">
          <span className="u-hiddenVisually">Centro</span>
          <SVGIcon
            logo
            iconName="logo-centro"
            className="App-logo"
            preserveAspectRatio="xMinYMin meet"
          />
        </a>

        <ul className="App-nav">
          <li className={searchNavClasses}>
            <a className="App-iconContainer App-globalSearchLink" title="Find property or vendor">
              <SVGIcon
                className="App-icon"
                iconName="icon-search"
                style={{ left: 12 }}
              />
            </a>
          </li>
        </ul>
      </div>
    );
  }
}
