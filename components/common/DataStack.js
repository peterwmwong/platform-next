import ReactTransitionGroup from 'react-addons-transition-group';
import markTerms from './mark-terms';
import cx from 'classnames';
import InlineLoader from './InlineLoader';

const StackCell = React.createClass({
  propTypes: {
    valueClassName : React.PropTypes.string,
    labelClassName : React.PropTypes.string,
    cell           : React.PropTypes.object.isRequired,
    align          : React.PropTypes.oneOf(['left', 'right']).isRequired,
    columnLabel    : React.PropTypes.string,
    focusedValues  : React.PropTypes.object,
    labelWidth     : React.PropTypes.number,
    markTerms      : React.PropTypes.string,
    loaderCondition: React.PropTypes.bool
  },

  getInitialState() {
    return { highlight: false };
  },

  componentWillEnter(callback) {
    this.setState({
      highlight: true
    });
    callback();
  },

  renderValue() {
    const { cell, loaderCondition } = this.props;
    const valueClassName = cx('DataStack-value', this.props.valueClassName, cell.valueClassName);
    const value = typeof cell.value === 'object' ? cell.value : markTerms(cell.value, this.props.markTerms, 'u-textHighlight');
    const titleValue = cell.title ? cell.title : '';

    if (loaderCondition === true || loaderCondition === false) {
      return (
        <InlineLoader
          className={valueClassName}
          condition={loaderCondition}
        >
          { value }
        </InlineLoader>
      );
    }
    else {
      return (
        <span className={valueClassName} title={titleValue}>
          { value }
        </span>
      );
    }
  },

  render() {
    const { cell, labelClassName } = this.props;

    const cellStyle = {};
    cellStyle[this.props.align === 'right' ? 'paddingRight' : 'paddingLeft'] = this.props.labelWidth;

    const cellClassName = cx('DataStack-cell', this.props.cellClassName, cell.className, {
      'is-focused': this.props.focusedValues && this.props.focusedValues[cell.valueId],
      'is-highlighted': this.state.highlight
    });

    return (
      <li
        key={cell.valueId}
        className={cellClassName}
        style={cellStyle}
      >
        <span style={{ width:this.props.labelWidth }} className={cx('DataStack-label', labelClassName )}>{cell.label}</span>
        { this.renderValue() }
      </li>
    );
  }
});

const DataStack = React.createClass({
  propTypes: {
    align          : React.PropTypes.oneOf(['left', 'right']),
    cells          : React.PropTypes.array,
    style          : React.PropTypes.object,
    className      : React.PropTypes.string,
    labelClassName : React.PropTypes.string,
    valueClassName : React.PropTypes.string,
    markTerms      : React.PropTypes.string
  },

  getDefaultProps() {
    return {
      align: 'right'
    };
  },

  renderCells() {
    const cells = this.props.cells
      .filter(cell => {
        return (typeof cell.canShow !== 'function' || cell.canShow()) && typeof cell === 'object';
      })
      .map((cell) => (
        <StackCell
          key={cell.valueId}
          cell={cell}
          {...this.props}
        />
      ));

    return (
      <ReactTransitionGroup>
        {cells}
      </ReactTransitionGroup>
    );
  },

  render() {

    const className = cx('DataStack', this.props.className, {
      'DataStack--right': this.props.align === 'right'
    });

    return (
      <ul
        ref="container"
        className={className}
        style={this.props.style}
      >
        {this.renderCells()}
      </ul>
    );
  }
});

module.exports = DataStack;
