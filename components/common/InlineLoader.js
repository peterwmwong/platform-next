export default React.createClass({
  propTypes: {
    // Indicates whether the loading indicator should be shown.
    condition: React.PropTypes.bool.isRequired,
    className: React.PropTypes.string
  },

  render: function() {
    return this.props.condition ? this.renderLoader() : this.renderContent();
  },

  renderLoader: function() {
    const className = `${ this.props.className || '' } Loader--inline Loader-ellipsis`;
    return (
      <span className={className}>
        <span className="Loader-ellipsisDot">.</span>
        <span className="Loader-ellipsisDot">.</span>
        <span className="Loader-ellipsisDot">.</span>
      </span>
    );
  },

  renderContent: function() {
    return <span className={this.props.className}>{this.props.children}</span>;
  }
});
