const SVGIcon = ({
  iconName,
  logo,
  path=(logo ? '/client/logos.svg' : '/client/icons.svg'),
  ...svgProps
}) => (
  <svg {...svgProps}>
    <use xlinkHref={`${path}#${iconName}`} />
  </svg>
);

SVGIcon.propTypes = {
  iconName: React.PropTypes.string.isRequired,
  path    : React.PropTypes.string,
  logo    : React.PropTypes.bool
};

export default SVGIcon;