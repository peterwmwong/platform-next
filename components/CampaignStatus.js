import cx from 'classnames';

function colorForStatus(status = '') {
  switch (status.toLowerCase()) {
    case 'planning': return 'u-textColorWarning';
    case 'approved': return 'u-textColorSecondaryMedShaded';
    case 'live':     return 'u-textColorConfirm';
    case 'archived': return 'u-textColorMediumShaded';
  }
}

export default ({ status }) => (
  <div className="CampaignStatus u-paddingTopBottom2 u-paddingLeftRight1 u-textRight">
    <h1 className={cx('u-textAllCaps u-marginBottom1', colorForStatus(status))}>
      {status}
    </h1>
    <div className="u-textColorMedium u-textSizeSmall u-textHeight2pt5">
      Campaign status
    </div>
  </div>
);
