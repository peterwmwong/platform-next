import cx from 'classnames';
import formatters from '../helpers/formatters';

const SUBHEADING_CLASS = 'List-item u-textColorMedium';

const CampaignTitle = ({
  title,
  clientName,
  brandName,
  startDate,
  endDate,
  ugcid
}) => (
  <div className="u-paddingLeftRight1 u-paddingTopBottom2">
    <h1 className="CampaignHeader-title u-marginBottom1">
      {title || <span>&nbsp;</span>}
    </h1>
    <ul className="List List--horiz u-textColorLight">
      <li className={SUBHEADING_CLASS}>
        {clientName}
      </li>
      <li className={SUBHEADING_CLASS}>
        {brandName}
      </li>
      <li className={cx('CampaignHeader-dates', SUBHEADING_CLASS)}>
        {formatters.date(startDate)}
        &nbsp;-&nbsp;
        {formatters.date(endDate)}
      </li>
      {ugcid &&
        <li className={SUBHEADING_CLASS}>
          ID: {ugcid}
        </li>
      }
    </ul>
  </div>
);

CampaignTitle.propTypes = {
  title: React.PropTypes.string,
  clientName: React.PropTypes.string,
  brandName: React.PropTypes.string,
  startDate: React.PropTypes.string,
  endDate: React.PropTypes.string,
  ugcid: React.PropTypes.string
};

export default CampaignTitle;
