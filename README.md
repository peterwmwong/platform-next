# platform-next

A test bed for a different frontend architecture to the Platform.

## Goals

- Faster/easier feature development
- Faster/easier debugging
- Faster/smaller frontend

## Experiment #1: Next.js / React.js (or Preact or Inferno)

Next.js is a convention based framework that introduces a Page React Component.
A Page Component is simply a React Component defined in the `pages/` directory
and automatically gets the following:

- Automatic Routing (each Page is a route)
- Automatic Server Side Rendering
        - Includes a one method API addition to a allow data fetching aswell
- Automatic Code Splitting
        - Only includes modules imported by the Page (also transitive imports)
        - Only requests the difference when navigating to another Page
        - This *greatly* reduces the page load size and load time for a "Page"
- Automatic Prefetching of other pages linked by the current page
- Automatic Service Worker
        - This is huge... once a page is loaded or prefetched, a browser refresh
      uses hits Service Worker (cache) instead of a slow call to the server.
        - No internet, no problem.  Essentially allows for offline support aswell.

Next.js also provides other utilities that enhance ALL React components:

- Programatic Routing (location redirect/push)
- Out-of-the-box Hot Module Replacement
- Out-of-the-box Error view
        - Ever make a change and wonder why the browser hasn't updated...
      hit refresh, blank screen, wtf, check dev tools, check terminal... 
      oh fuck me I forgot a closing brace...
        - What if the browser just showed the error's what and where? 
      That's what this is.

### Examples

### / (index) List Campaigns

```js
// page/index.js
import DataPage from '../helpers/DataPage.js';

export default DataPage(
  'api/direct/campaign_dashboard_items?my_campaigns=false&page=1&pageSize=25&statuses%5B%5D=Planning&statuses%5B%5D=Approved&statuses%5B%5D=Live',
  ({ results:campaigns }) =>
    <div>
      <h1>Campaigns</h1>
      <ul>
        {campaigns.map(c => (
          <li key={c.id}>{c.campaign_name}</li>
        ))}
      </ul>
    </div>
)
```

### /markets List Markets

```js
// page/markets.js
import DataPage from '../helpers/DataPage.js';

export default DataPage(
  'api/catalog/markets',
  markets =>
    <div>
      <h1>Markets</h1>
      <ul>
        {markets.map(m => (
          <li key={m.id}>{m.name}</li>
        ))}
      </ul>
    </div>
)
```

### Meeting the Goals

- Faster/easier feature development
    - Simplifies requesting and rendering data
        - No Transis Model, Transis Mapper, Statechart, AppState, React State Mixin
        - Literally one line of code above your React component
        - Pit of success: incestivizes being lazy and do *as little as possible* data manipulation on the frontend
            - Currently a data can be created/transformed in the Transis Model, Transis Mapper or a Statechart State
            - In most cases the frontend is doing waaaayyy to much, where the server could have done it faster/cheaper
            - This also lead to too many approaches (Corey code vs Peter Code vs Devin Code etc.)
                - "I'd do that in the Model... but for this... the Mapper seems right... (hand waving)"
                - Just examining your options take time, but if you knew there was only one way... think of the
          cognitive/time savings.
    - Simplifies (through enforced convention) page / route organization...
        - No complicated nested/concurrent Statechart
        - A React component in the `pages/` directory is a page and gets route with the same name. That's it.
- Faster/easier debugging
    - Simplifies answering "Where's this bad data coming from?"
        - Currently you'd need to check: Transis Model, Transis Mapper, Statechart, AppState, React State Mixin
            - That's alot of places to check...
        - With Next.js, there's only have one place to check: first line of the Page Component
      (ex. `async ({ get }) => await get('/api/where/the/data/came/from')`)
- Faster/smaller frontend
    - Next.js only includes the modules imported by a Page
        - because Next.js uses webpack 2, tree shaking should be possible aswell (only used parts of a module)
    - Next.js when navigating to another page, only the missing modules are requested
    - Next.js does this all automatically, no need to hand craft a bundle (and accidentally screw it up)


## Getting Started

1. Make sure the platform is running on port 3000 and available at `localhost:3000` (default `bundle exec rails s`)
2. Make sure you have Node.js +7.4 [Node.js Current](https://nodejs.org/en/download/current/)
3. `npm install`
4. `npm run dev`
5. Visit and login `http://localhost:3003/auth/login`

## TODO

- Handling complex state
    - Forms
    - Dialogs