import DataPage from '../helpers/DataPage.js';

export default DataPage(
  'api/catalog/markets',
  (markets = []) =>
    <div>
      <h1>Markets</h1>
      <ul>
        {markets.map(m =>
          <li key={m.id}>{m.name}</li>
        )}
      </ul>
    </div>
)
