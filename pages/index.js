import React from 'react';

export default class extends React.Component {
  static async getInitialProps({ req, res }) {
    // If not successful (for whatever reason), redirect to `/auth/login`
    // TODO: Let's verify and match the current Platform behavior.
    // Server redirect
    if(req) {
      res.statusCode = 302;
      res.setHeader('location', '/dashboard');
      res.end('');
      return res;
    }
    // Client redirect
    else window.location = '/dashboard';
  }
}