import Link from 'next/prefetch';

import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer';
import List from 'react-virtualized/dist/commonjs/List';
import CellMeasurer from 'react-virtualized/dist/commonjs/CellMeasurer';

import markTerms from '../components/common/mark-terms';
import DataStack from '../components/common/DataStack';
import DataPage from '../helpers/DataPage';
import gridColumnStyles from '../helpers/gridColumnStyles';
import formatters from '../helpers/formatters';
import capitalize from '../helpers/capitalize';

const COL_WIDTHS = [400, 108, 80, 168, 154, 410];
const COL_STYLES = gridColumnStyles(COL_WIDTHS);
const REASON_MAPPING = {
  budget_cut: 'Budget Cut',
  duplicate: 'Duplicate Campaign',
  not_approved: 'Not Approved',
  on_hold: 'On-hold',
  strategy_change: 'Strategy Change',
  other: 'Other'
};

const campaignBudgetType = status =>
  status === 'planning' || status === 'archived'
    ? 'target'
    : 'approved';

const remainingContent = vendors =>
  vendors && vendors.length
    ? <div className="u-paddingLeftRight0pt25 u-paddingTopBottom0pt5">
        {vendors.map((vendor, i) => <p key={i}>{vendor}</p>)}
      </div>
    : '--'

const Row = ({ index, style, campaign, searchTerms='' }) => (
  <div className="DataUltima-row DataUltima-row--clickable HoverContainer" style={style}>
    <Link href={`/campaigns?id=${campaign.campaign_id}`}><a className="AgencyDashboard-campaignLink u-posFillContainer" /></Link>
    <div className="DataUltima-col AgencyDashboard-nameCol u-textBreakAll" style={COL_STYLES[0]}>
      <div className="Flex">
        <div>
          {markTerms(campaign.campaign_name, searchTerms)}
          <div className="AgencyDashboard-ugcid u-textColorLight u-textSizeXSmall u-textTruncate u-textAllCaps">
            {markTerms(campaign.campaign_ugcid, searchTerms) || '--'}
          </div>
        </div>
      </div>
    </div>
    <div className="DataUltima-col AgencyDashboard-datesCol" style={COL_STYLES[1]}>
      <DataStack
        align="left"
        labelWidth={38}
        cells={[
          { valueId: 'startDate', label: 'starts', value: markTerms(formatters.date(campaign.campaign_current_start_date), searchTerms) },
          { valueId: 'endDate',   label: 'ends',   value: markTerms(formatters.date(campaign.campaign_current_end_date), searchTerms) }
        ]}
      />
    </div>
    <div className="DataUltima-col AgencyDashboard-statusCol u-posRelative" style={COL_STYLES[2]}>
      {capitalize(campaign.campaign_status) || '--'}
      {campaign.campaign_is_archived &&
        <div>
          {!campaign.campaign_archived_notes
            ? REASON_MAPPING[campaign.campaign_archived_reason]
            : <CUI.Tooltip
                tooltipClassNames="u-maxWidth17"
                content={campaign.campaign_archived_notes}
                attachment="top left"
                targetAttachment="bottom left"
              >
                <span className="u-linkUnderlineDotted">{REASON_MAPPING[campaign.campaign_archived_reason]}</span>
              </CUI.Tooltip>
          }
        </div>
      }
    </div>
    <div className="DataUltima-col AgencyDashboard-budgetCol" style={COL_STYLES[3]}>
      <DataStack
        align="right"
        labelWidth={66}
        cells={[
          { valueId: 'budget', label: campaignBudgetType(campaign.campaign_status), value: formatters.currency(campaign.campaign_budget) },
          campaign.campaign_status === 'live' && {
            valueId: 'projectedBalance',
            label: 'proj. balance',
            valueClassName: campaign.campaign_projected_balance_warning ? 'u-textWeightBold u-textColorAlert' : '',
            value: formatters.currency(campaign.campaign_current_projected_balance_client)
          }
        ]}
      />
    </div>
    <div className="DataUltima-col AgencyDashboard-remainingCol u-textCenter" style={COL_STYLES[4]}>
      <DataStack
        align="right"
        labelWidth={66}
        cells={[
          {
            valueId: 'remainingRevisions',
            label: 'revision',
            value: remainingContent(campaign.campaign_vendors_with_remaining_revisions)
          },
          {
            valueId: 'remainingIOs',
            label: 'IO',
            value: remainingContent(campaign.campaign_vendors_with_remaining_ios)
          },
          {
            valueId: 'remainingRfps',
            label: 'RFP',
            value: remainingContent(campaign.campaign_vendors_with_remaining_rfps)
          }
        ]}
      />
    </div>
    <div className="DataUltima-col AgencyDashboard-notesCol" style={COL_STYLES[5]}>
      <div>
        {markTerms(campaign.campaign_dashboard_note || '--', searchTerms)}
      </div>
      <a className="u-textSizeXSmall u-linkSecondary u-linkIncreaseHitArea">
        Edit
      </a>
    </div>
  </div>
);

const CampaignList = ({ campaigns, totalCount }) => {
  const renderRow = props => <Row {...props} campaign={campaigns[props.index]} />;
  return (
    <AutoSizer>
      {({ height, width }) => (
        <CellMeasurer
          cellRenderer={renderRow}
          columnCount={1}
          rowCount={totalCount}
          width={width}
        >
          {({ getRowHeight }) => (
            <List
              height={height}
              rowCount={totalCount}
              rowHeight={getRowHeight}
              rowRenderer={renderRow}
              width={width}
            />
          )}
        </CellMeasurer>
      )}
    </AutoSizer>
  )
};

export default DataPage(
  'api/direct/campaign_dashboard_items?my_campaigns=false&page=1&pageSize=25&statuses%5B%5D=Planning&statuses%5B%5D=Approved&statuses%5B%5D=Live',
  ({ results, meta: { total_count } }) =>
    <div className="AgencyDashboard Flex Flex--vertical u-posFillContainer">
      <div className="Flex-fit">
        <div className="Arrange Arrange--middle u-decoBorderBottomLightTinted u-height15">
          <div className="Arrange-sizeFill u-paddingLeft0pt75">
            <h1>Dashboard</h1>
          </div>
        </div>
      </div>
      <div className="Flex-grow u-posRelative">
        <CampaignList campaigns={results} totalCount={total_count} />
      </div>
    </div>
)
