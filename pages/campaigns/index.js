import cx from 'classnames';
import DataPage from '../../helpers/DataPage';
import CampaignHeader from '../../components/CampaignHeader';

export default DataPage(
  ({ reqRes }) => reqRes.query.id,
  campaignId => {
    const dashboardClassName = cx('Flex-fit u-posRelative', {
      'u-decoBorderBottomLightTinted': false//TODO: !!campaignDashboard
    });

    return (
      <div className="CampaignOverview Flex Flex--vertical u-posFillContainer">
        <div className="CampaignOverview-header Flex-fit u-decoBorderBottomLightTinted">
          <CampaignHeader campaignId={campaignId} />
        </div>

        <div className={dashboardClassName}>
          {/*<Dashboard
            campaign={campaign}
            show={campaignDashboard}
            onCloseMessages={this.handleToggleMessages}
          />*/}
        </div>

        <div className="CampaignOverview-body Flex-grow u-posRelative">
          {/*<CampaignOverviewMain
            campaign={campaign}
            onSwitchMode={this.handleSwitchMode}
            onEditCampaign={this.handleEditCampaign}
            onArchiveCampaign={campaign.isArchived ? this.handleUnarchiveCampaign : this.handleArchiveCampaign}
            panel={campaignOverviewPanel}
            onSwitchPanel={this.handleSwitchPanel}
          />*/}
        </div>
      </div>
    );
  }
);
